
const axios = require('axios');

class YouTube {
    constructor(config, ytdl) {
        this.config = config;
        this.ytdl = ytdl;

        this.watchVideoUrl = 'https://www.youtube.com/watch?v=';
    }

    async search(searchQuery) {
        const getQuery = () => {
            const matches = /(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&"'>]+)/.exec(searchQuery);

            // If the query is a URL, extract the ID
            if (matches && typeof matches[5] !== 'undefined') {
                return matches[5];
            }

            return escape(searchQuery);
        };

        const apiKey = this.config.youtubeApiKey;
        if (!apiKey) {
            throw 'YouTube API key is not set.';
        }

        const requestUrl = `https://www.googleapis.com/youtube/v3/search?part=snippet&q=${getQuery()}&key=${apiKey}`;

        const response = await axios.get(requestUrl);

        if (response.data) {
            const data = response.data;

            if (data.items.length == 0) {
                throw 'Could not find anything to play';
                return;
            }

            for (const item of data.items) {
                if (item.id.kind === 'youtube#video') {
                    const info = await this.ytdl.getBasicInfo(item.id.videoId);

                    return info;
                }
            }

            return null;
        } else {
            throw 'Unexpected error when searching YouTube';
        }
    }
}

module.exports = (...args) => new YouTube(...args);

