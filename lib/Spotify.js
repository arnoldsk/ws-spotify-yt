
const axios = require('axios');

class Spotify {
    constructor(config, ytdl) {
        this.config = config;
        this.ytdl = ytdl;
    }

    async getOAuth() {
        const spotifyClient = this.config.spotifyClient;
        const spotifyClientBase64 = Buffer.from(`${spotifyClient.id}:${spotifyClient.secret}`).toString('base64');
        const spotifyOAuthResponse = await axios({
            url: `https://accounts.spotify.com/api/token`,
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${spotifyClientBase64}`,
            },
            params: {
                'grant_type': 'client_credentials',
            },
        });

        return spotifyOAuthResponse.data;
    }

    async getNowPlayingData(authorizeToken) {
        const spotifyClient = this.config.spotifyClient;

        const spotifyTrackResponse = await axios({
            url: `https://api.spotify.com/v1/me/player/currently-playing`,
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${authorizeToken}`,
            },
        });
        const spotifyData = spotifyTrackResponse.data;

        return spotifyData;
    }
}

module.exports = (...args) => new Spotify(...args);

