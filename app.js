const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const ytdl = require('ytdl-core');

const config = require('./config');
const yt = require('./lib/YouTube')(config, ytdl);
const spotify = require('./lib/Spotify')(config);

app.use(express.static('public'));
app.set('view engine', 'ejs');

/**
 * Storage
 */
const users = {};

/**
 * Routes
 */
app.get('/', (req, res) => {
    res.render('index', {
        url: config.url,
    });
});

app.get('/login', function(req, res) {
    const redirectUrl = config.url;
    const scopes = 'user-read-currently-playing';

    // TODO should be in Spotify class
    // TODO renew this shit
    res.redirect('https://accounts.spotify.com/authorize'
        + '?response_type=token'
        + '&client_id=' + config.spotifyClient.id
        + (scopes ? '&scope=' + encodeURIComponent(scopes) : '')
        + '&redirect_uri=' + encodeURIComponent(redirectUrl));
});

/**
 * Sockets
 */
io.on('connection', (socket) => {
    /**
     * User has joined
     */
    users[socket.id] = {
        nowPlayingSpotifyId: '',
        nowPlayingIval: 0,
    };

    /**
     * User has left
     */
    socket.on('disconnect', () => {
        clearInterval(users[socket.id].nowPlayingIval);
        delete users[socket.id];
    });

    socket.on('start', (hash) => {
        if (!hash.length) {
            io.to(socket.id).emit('login');
            return;
        } else {
            users[socket.id].nowPlayingSpotifyId = '';
        }

        try {
            const checkNowPlaying = async () => {
                const nowPlayingData = await spotify.getNowPlayingData(hash);

                // If not the same song as already playing
                if (users[socket.id].nowPlayingSpotifyId === nowPlayingData.item.id) {
                    return;
                } else {
                    users[socket.id].nowPlayingSpotifyId = nowPlayingData.item.id;
                }

                // find the youtube video of this
                // if its not playing yet, do not autplay
                // if its is_playing true then send to start the video
                // the time would prolly not match but anyways, pass it along
                const videoName = `${nowPlayingData.item.artists[0].name} - ${nowPlayingData.item.name}`;
                const videoData = await yt.search(videoName);

                const time = {
                    total: nowPlayingData.item.duration_ms / 1000,
                    current: nowPlayingData.progress_ms / 1000,
                    remaining: (nowPlayingData.item.duration_ms - nowPlayingData.progress_ms) / 1000,
                };

                io.to(socket.id).emit('youtube video', {
                    id: videoData.video_id,
                    autoplay: nowPlayingData.is_playing,
                    time,
                });
            };

            checkNowPlaying();
            // Check for now playing every x seconds
            users[socket.id].nowPlayingIval = setInterval(checkNowPlaying, 5 * 1000);
        } catch (e) {
            console.error(e);
        }
    });
});

http.listen(config.port, () => {
    console.log(`listening on *:${config.port}`);
});
